#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the U-BootMemOps contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import os
import serial
import struct
import sys
import time

class MemOps():
    def __init__(self):
        self.ttyBaudrate = 115200
        self.ttyDevName = "/dev/ttyUSB0"
        self.ttyDev = None

        self.memOpStartAddr = 0x10000000
        self.memOpSize = 1024 * 128
        self.memOpBurstSize = 1024
        self.memOpWordSize = 4

        # Used to check if U-Boot has finished executing a command.
        self.uBootPrompt = "U-Boot#"
        self.uBootMemReadCmd = "md"
        self.uBootMemWriteCmd = "mw"
        self.uBootMemCmdSuffix = ".l"
        # Used to check if U-Boot is responding.
        self.uBootNopCmd = " "

        self.outFileName = "dump-0x{0:08X}.bin".format(self.memOpStartAddr)


    def __del__(self):
        self.atExit()

    def atExit(self):
        try:
            self.ttyDev.close()
        except Exception:
            pass

    def uBootRun(self, cmd):
        cmd += bytes("\n", "ascii")
        print("Executing:", cmd)
        self.ttyDev.write(cmd)

        buf = bytes()
        tmpBuf = bytes()
        while True:
            tmpBuf = self.ttyDev.read(self.ttyDev.inWaiting() + 16)
            if tmpBuf == bytes():
                break
            buf += tmpBuf

        # Normalize newlines
        buf = buf.replace(bytes("\r\r\n", "ascii"), bytes("\n", "ascii"))
        buf = buf.replace(bytes("\r\n", "ascii"), bytes("\n", "ascii"))

        # Remove echoed cmd
        bufSlice = buf.rpartition(cmd)
        bufSlice = bufSlice[2]

        bufLines = buf.splitlines()
        # Check if the last line is a U-Boot prompt.
        if len(bufLines) < 1 or bytes(self.uBootPrompt, "ascii") not in bufLines[-1]:
            print("U-Boot not responding correctly.")
            sys.exit(1)

        return bufSlice

    def readMem(self):
        curAddr = self.memOpStartAddr
        finishedByteCount = 0
        startTime = time.time()
        self.outFile = open(self.outFileName, "wb")

        while (finishedByteCount < self.memOpSize):
            curCmd = bytes(self.uBootMemReadCmd + self.uBootMemCmdSuffix +
                           " 0x{0:08X} 0x{1:X}".format(curAddr, self.memOpBurstSize),
                           "ascii")
            buf = self.uBootRun(curCmd)
            bufLines = buf.splitlines()

            for line in bufLines:
                #Skip lines that don't have an address in them. Addresses end with ":".
                if((line == bytes()) or (bytes(":", "ascii") not in line)):
                    continue
                linePieces = line.split()

                # Sanity check. First part must contain address.
                if(linePieces[0].endswith(bytes(":", "ascii")) == False):
                    print("Invalid line structure. Probably an IO error.")
                    sys.exit(1)

                # Sanity check. Calculated address must be equal to read address.
                try:
                    if(int(linePieces[0][:-1], 16) != curAddr):
                        print("Error: current address != line address."
                              " Probably an IO error.")
                        sys.exit(1)
                except ValueError:
                    print("Error while parsing line address. Aborting.")
                    sys.exit(1)

                print("Line address: " + str(linePieces[0][:-1]))
                print("Line pieces: ", end=" ")
                try:
                    for i in range(1, (16 // self.memOpWordSize) + 1):
                        print(linePieces[i], end=" ")
                        self.outFile.write(struct.pack(self.bytePackFormat,
                                           int(linePieces[i], 16)))
                        finishedByteCount += self.memOpWordSize
                    print()
                except ValueError:
                    print("Error while parsing data. Aborting.")
                    sys.exit(1)

                curAddr = self.memOpStartAddr + finishedByteCount

            print()

        self.outFile.close()
        print("Finished reading.")
        print("Total bytes: {0:d}".format(finishedByteCount))
        rate = (finishedByteCount / (time.time() - startTime)) / 1024
        print("Rate: {0:.2g} kB/s".format(rate))
        print()

    def writeMem(self):
        curAddr = self.memOpStartAddr
        finishedByteCount = 0
        startTime = time.time()
        self.inFile = open(self.inFileName, "rb")

        while (finishedByteCount < self.memOpSize):
            curBytes = self.inFile.read(self.memOpWordSize)
            if (curBytes == b"" or len(curBytes) < self.memOpWordSize):
                print("Error: couldn't read when file shouldn't have ended. Aborting.")
                sys.exit(1)

            curCmd = bytes(self.uBootMemWriteCmd + self.uBootMemCmdSuffix +
                           " 0x{0:08X} 0x".format(curAddr), "ascii")

            # TODO Needs testing on other systems and architectures
            curBytes = bytearray(curBytes)
            curBytes.reverse()
            curBytes = bytes(curBytes)
            for i in range(self.memOpWordSize):
                curCmd += bytes("{0:02X}".format(curBytes[i]), "ascii")

            self.uBootRun(curCmd)
            finishedByteCount += self.memOpWordSize
            curAddr = self.memOpStartAddr + finishedByteCount

        self.inFile.close()
        print("Finished writing.")
        rate = (finishedByteCount / (time.time() - startTime)) / 1024
        print("Rate: {0:.2g} kB/s".format(rate))
        print()

    def verifyMem(self, inFileName):
        curAddr = self.memOpStartAddr
        finishedByteCount = 0
        startTime = time.time()
        self.inFile = open(self.outFileName, "rb")

        while (finishedByteCount < self.memOpSize):
            curCmd = bytes(self.uBootMemReadCmd + self.uBootMemCmdSuffix +
                           " 0x{0:08X} 0x{1:X}".format(curAddr, self.memOpBurstSize),
                           "ascii")
            buf = self.uBootRun(curCmd)
            bufLines = buf.splitlines()

            for line in bufLines:
                #Skip lines that don't have an address in them. Addresses end with ":".
                if((line == bytes()) or (bytes(":", "ascii") not in line)):
                    continue
                linePieces = line.split()

                # Sanity check. First part must contain address.
                if(linePieces[0].endswith(bytes(":", "ascii")) == False):
                    print("Invalid line structure. Probably an IO error.")
                    sys.exit(1)

                # Sanity check. Calculated address must be equal to read address.
                try:
                    if(int(linePieces[0][:-1], 16) != curAddr):
                        print("Error: current address != line address."
                              " Probably an IO error.")
                        sys.exit(1)
                except ValueError:
                    print("Error while parsing line address. Aborting.")
                    sys.exit(1)

                print("Line address: " + str(linePieces[0][:-1]))
                print("Line pieces: ", end=" ")
                try:
                    for i in range(1, (16 // self.memOpWordSize) + 1):
                        print(linePieces[i], end=" ")
                        curFileBytes = self.inFile.read(self.memOpWordSize)
                        if (curFileBytes == b""):
                            print("Error: couldn't read when file shouldn't have"
                                  " ended. Aborting.")
                            sys.exit(1)
                        curMemBytes = struct.pack(self.bytePackFormat,
                                                  int(linePieces[i], 16))
                        if(curFileBytes != curMemBytes):
                            print("\nDifference found at: 0x{0:08X}".format(
                                  self.memOpStartAddr + finishedByteCount),
                                  "File data: 0x" + curFileBytes.hex(),
                                  "Mem data: 0x" + curMemBytes.hex())
                            sys.exit(1)
                        finishedByteCount += self.memOpWordSize
                    print()
                except ValueError:
                    print("Error while parsing data. Aborting.")
                    sys.exit(1)

                curAddr = self.memOpStartAddr + finishedByteCount

            print()

        self.inFile.close()
        print("Finished verifying. No differences found.")
        print("Total bytes: {0:d}".format(finishedByteCount))
        rate = (finishedByteCount / (time.time() - startTime)) / 1024
        print("Rate: {0:.2g} kB/s".format(rate))
        print()

    def main(self):
        print("U-BootMemOps: Memory operations for U-Boot.")
        print("Copyright 2018,")
        print("Ahmad Draidi and the U-BootMemOps contributors.\n")

        parser = argparse.ArgumentParser(description="Do operations on memory "
                                         "through the U-Boot shell.")

        parser.add_argument("address", help="Memory operation start address.",
                            metavar="StartAddress")
        parser.add_argument("-r", "--read", help="Read memory to output file.", action="store_true")
        parser.add_argument("-w", "--write", help="Write memory from input file.", action="store_true")
        parser.add_argument("-v", "--verify", help="Verify memory against input file.", action="store_true")

        parser.add_argument("-s", "--size", help="Number of bytes to work on."
                            " Default is {0:d} bytes.".format(self.memOpSize),
                            metavar="Bytes")
        parser.add_argument("-t", "--burstsize", help="Number of bytes per"
                            " single burst. Must be a multiple of WordSize. Default is "
                            "{0:d}.".format(self.memOpBurstSize), metavar="BurstSize")
        parser.add_argument("-u", "--wordsize", help="Number of bytes per"
                            " word.  Can be either 1, 2 or 4. Default is "
                            "{0:d}.".format(self.memOpWordSize), metavar="WordSize")

        parser.add_argument("-o", "--outfile", help="Output file name."
                            " Default is \"{0:s}\", where 0x{1:08X} is replaced"
                            " with the StartAddress.".format(self.outFileName,
                            self.memOpStartAddr), metavar="OutFileName")
        parser.add_argument("-i", "--infile", help="Input file name.",
                            metavar="InFileName")

        parser.add_argument("-b", "--baudrate", help="TTY baudrate to use. "
                            "Default is {0:d}.".format(self.ttyBaudrate),
                            metavar="Baudrate")
        parser.add_argument("-d", "--ttydevice", help="TTY device to use."
                            " Default is \"{0:s}\".".format(self.ttyDevName),
                            metavar="Device")
        parser.add_argument("-p", "--prompt", help="U-Boot shell prompt."
                            " Default is \"{0:s}\".".format(self.uBootPrompt),
                            metavar="PromptString")
        args = parser.parse_args()

        try:
            self.memOpStartAddr = int(args.address, 0)
        except ValueError:
            print("Invalid start address.")
            sys.exit(1)

        if args.prompt != None:
            self.uBootPrompt = args.prompt

        if args.ttydevice != None:
            self.ttyDevName = args.ttydevice

        if args.baudrate != None:
            try:
                self.ttyBaudrate = int(args.baudrate, 0)
            except ValueError:
                print("Invalid baudrate.")
                sys.exit(1)

        if args.size != None:
            try:
                self.memOpSize = int(args.size, 0)
            except ValueError:
                print("Invalid size.")
                sys.exit(1)

        if args.burstsize != None:
            try:
                self.memOpBurstSize = int(args.burstsize, 0)
            except ValueError:
                print("Invalid burst size.")
                sys.exit(1)

        if args.wordsize != None:
            try:
                self.memOpWordSize = int(args.wordsize, 0)
            except ValueError:
                print("Invalid word size.")
                sys.exit(1)

        if((self.memOpWordSize * self.memOpBurstSize) < 16):
            print("Word size * burst size must be a minimum of 16 bytes.")
            sys.exit(1)

        if(self.memOpSize < (self.memOpBurstSize * self.memOpWordSize)):
            print("Size must be greater than (burst size * word size).")
            sys.exit(1)

        if(self.memOpSize % (self.memOpBurstSize * self.memOpWordSize) != 0):
            print("Size must be a multiple of (burst size * word size).")
            sys.exit(1)

        if self.memOpWordSize == 1:
            self.uBootMemCmdSuffix = ".b"
        elif self.memOpWordSize == 2:
            self.uBootMemCmdSuffix = ".w"
        elif self.memOpWordSize == 4:
            self.uBootMemCmdSuffix = ".l"
        else:
            print("Invalid word size. 1, 2 or 4 only allowed.")
            sys.exit(1)

        if(self.memOpWordSize == 1):
            self.bytePackFormat = "<B"
        elif(self.memOpWordSize == 2):
            self.bytePackFormat = "<H"
        elif(self.memOpWordSize == 4):
            self.bytePackFormat = "<I"

        if(args.read and args.write):
            print("Please specify read or write operation only.")
            sys.exit(1)
        if(not args.read and not args.write and not args.verify):
            print("Please specify read, write or verify operation.")
            sys.exit(1)

        if(args.verify and not args.read and args.infile == None):
            print("Please specify an input file to verify against.")
            sys.exit(1)

        self.memOpRead = False
        self.memOpWrite = False
        self.memOpVerify = False

        if(args.read):
            self.memOpRead = True
        elif(args.write):
            self.memOpWrite = True

        if(args.verify):
            self.memOpVerify = True

        if(args.outfile != None):
            self.outFileName = args.outfile
        if(args.infile != None):
            self.inFileName = args.infile

        try:
            self.ttyDev = serial.Serial(self.ttyDevName, self.ttyBaudrate,
                                        timeout=1)
        except serial.SerialException as e:
            print(str(e))
            sys.exit(1)

        ttySettingsDict = self.ttyDev.getSettingsDict()
        # I guess we need to wait after opening TTY in some cases. I don't remember anymore.
        time.sleep(1)

        self.ttyDev.flushInput()
        self.ttyDev.flushOutput()

        ttySettingsDict["timeout"] = 0.5
        self.ttyDev.applySettingsDict(ttySettingsDict)

        # Check if we're connected to U-Boot.
        for i in range(2):
            buf = self.uBootRun(bytes(self.uBootNopCmd, "ascii"))
            if bytes(self.uBootPrompt, "ascii") in buf:
                print("U-Boot responding.")
            else:
                print("U-Boot not responding.")
                sys.exit(1)
        print()

        # Use shorter timeout between reads
        ttySettingsDict["timeout"] = 0.01
        self.ttyDev.applySettingsDict(ttySettingsDict)

        if (self.memOpRead):
            print("Output file name: {0:s}".format(self.outFileName))
            if(os.path.exists(self.outFileName)):
                print("Output file already exists. Aborting.")
                sys.exit(1)
        elif(self.memOpWrite):
            self.memOpSize = os.path.getsize(self.inFileName)
            print("Input file name: {0:s}, size: {1:d} bytes.".format(self.inFileName,
                  self.memOpSize))

        print("Start address: 0x{0:08X}, ".format(self.memOpStartAddr), end="")
        print("op. size: {0:d} bytes, ".format(self.memOpSize), end="")
        if (self.memOpRead or self.memOpVerify):
            print("burst size: {0:d}, ".format(self.memOpBurstSize), end="")
        print("word size: {0:d}.".format(self.memOpWordSize))
        print("")

        if((self.memOpSize % self.memOpWordSize > 0) and self.memOpWrite):
            print("Warning: Write size is not a multiple of word size. Extra "
                  "bytes will be filled with zeroes.")

        if(self.memOpWrite):
            answer = input("This will write memory on U-Boot device.\n"
                           "This could brick your device. Are you sure you want to continue? ")
            answer = answer.lower()
            if (answer != "y" and answer != "yes"):
                print("Aborting.")
                sys.exit(0)

        if(self.memOpRead):
            self.readMem()
        elif(self.memOpWrite):
            self.writeMem()

        if(self.memOpVerify):
            if(self.memOpRead):
                self.verifyMem(self.outFileName)
            else:
                self.verifyMem(self.inFileName)

if __name__ == "__main__":
    mops = MemOps()
    sys.exitfunc = mops.atExit
    mops.main()
    print("Bye!")
