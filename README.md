# U-BootMemOps
U-BootMemOps is a small program that can read, write and verify a memory area through the U-Boot shell.  
It's useful for reading/writing images of small memory chips of a platform, where the installed U-Boot bootloader doesn't have any way of saving or sending a memory area to disk or over network, among other things.

### Installation
- git clone https://gitlab.com/ar2000jp/U-BootMemOps.git
- cd U-BootMemOps
- python3 -mvenv venv
- source ./venv/bin/activate
- pip install pyserial

### Usage example
- Write a file to an ODROID C1's RAM and verify it:
   - ./U-BootMemOps.py 0x10000000 -p "odroidc#" -w -v -i dump-0x10000000.bin
- Dump the SPI flash of a TP-LINK TL-MR3220 v1 to a file and verify it:
   - ./U-BootMemOps.py 0x9f000000 -p "ar7240>" -s 0x400000 -r -v

### License
Copyright 2018 Ahmad Draidi and the U-BootMemOps contributors.  
SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
